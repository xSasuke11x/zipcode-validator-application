﻿using System;
using System.Globalization;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hw3;

namespace Hw3
{
    public class Read
    {
        public static void ReadLines(string region)
        {
            //Read in each file and save the contents into a list depending on region
            List<string> officialList = null;
            if (region == "United States")
            {
                var zipCodes = File.ReadAllLines(@"zip.txt");
                List<string> zipList = new List<string>(zipCodes);
                officialList = zipList.ToList();
            }
            else if (region == "United Kingdom")
            {
                var postalCodes = File.ReadAllLines(@"postal.txt");
                List<string> postalList = new List<string>(postalCodes);
                officialList = postalList.ToList();
            }
            else if (region == "Australia")
            {
                var ausCodes = File.ReadAllLines(@"aus.txt");
                List<string> ausList = new List<string>(ausCodes);
                officialList = ausList.ToList();
            }
                     
            var file = File.ReadAllLines(@"codes.txt");
            List<string> fileList = new List<string>(file);

            //Validate the codes
            Validate.CheckCodes(officialList, fileList, region);
        }
        
    }
}
