﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//The application will print out which zipcodes failed validation.
namespace Hw3
{
    public interface IRegion
    {
        void Output(IEnumerable<string> aList);
    }

    public class UnitedStates : IRegion
    {
        public void Output(IEnumerable<string> notExist)
        {
            Console.WriteLine("The following zip code(s) do not exist in the United States: ");

            if (Validate.IsEmpty(notExist) == false)
            {
                foreach (String code in notExist)
                {
                    Console.WriteLine(code);
                }
            }
        }
    }

    public class UnitedKingdom : IRegion
    {
        public void Output(IEnumerable<string> notExist)
        {
            Console.WriteLine("The following postal code(s) do not exist in the United Kingdom: ");

            if (Validate.IsEmpty(notExist) == false)
            {
                foreach (String code in notExist)
                {
                    Console.WriteLine(code);
                }
            }
        }
    }

    public class Australia : IRegion
    {
        public void Output(IEnumerable<string> notExist)
        {
            Console.WriteLine("The following postal code(s) do not exist in Australia: ");

            if (Validate.IsEmpty(notExist) == false)
            {
                foreach (String code in notExist)
                {
                    Console.WriteLine(code);
                }
            }
        }
    }

    public static class Validate
    {
        public static void CheckCodes(IEnumerable<string> officialList, IEnumerable<string> fileCodes, string region)
        {
            //Create a new list containing codes that do not exist
            var notExist = officialList.Union(fileCodes).Except(officialList.Intersect(fileCodes));

            //Check to see which country you should check.
            if (region == "United States")
            {
                UnitedStates usObj = new UnitedStates();
                usObj.Output(notExist);
            }
            else if (region == "United Kingdom")
            {
                UnitedKingdom ukObj = new UnitedKingdom();
                ukObj.Output(notExist);
            }
            else if (region == "Australia")
            {
                Australia ausObj = new Australia();
                ausObj.Output(notExist);
            }
        }

        public static Boolean IsEmpty<T>(this IEnumerable<T> source)
        {
            if (source == null)
                return true;
            return !source.Any();
        }
    }
}
