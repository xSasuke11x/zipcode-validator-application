﻿/*
 *   Author: Kennedy Tran
 *   Class: CSI - 335 - 51
 *   Assignment: 4
 *   Date Assigned: 2/06/14
 *   Due Date: 5:30 PM @ 2/13/14
 *   Description: This program is a zip code/postal code validator. The user has their
 *                  own file THAT NEEDS TO BE NAMED "codes.txt" Then the program will
 *                  tell the user if the zip code is a valid zip code in the country
 *                  that they installed the client on. If the user's current location
 *                  is not in the United States, the United Kingdom, or Australia, 
 *                  then the program will close.
 *   Certification of Authenticity:
 *   I certify that this assignment is entirely my own work.
 *   
 * **************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Hw3
{
    public static class RegionAndLanguageHelper
    {
        #region Constants

        private const int GEO_FRIENDLYNAME = 8;

        #endregion

        #region Private Enums

        private enum GeoClass : int
        {
            Nation = 16,
            Region = 14,
        };

        #endregion

        #region Win32 Declarations

        [DllImport("kernel32.dll", ExactSpelling = true, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        private static extern int GetUserGeoID(GeoClass geoClass);

        [DllImport("kernel32.dll")]
        private static extern int GetUserDefaultLCID();

        [DllImport("kernel32.dll")]
        private static extern int GetGeoInfo(int geoid, int geoType, StringBuilder lpGeoData, int cchData, int langid);

        #endregion

        #region Public Methods

        /// Returns machine current location as specified in Region and Language settings.
        public static string GetMachineCurrentLocation()
        {
            int geoId = GetUserGeoID(GeoClass.Nation); ;
            int lcid = GetUserDefaultLCID();
            StringBuilder locationBuffer = new StringBuilder(100);
            GetGeoInfo(geoId, GEO_FRIENDLYNAME, locationBuffer, locationBuffer.Capacity, lcid);

            return locationBuffer.ToString().Trim();
        }

        #endregion
    }
    public class Program
    {
        static void Main(string[] args)
        {
            string region = RegionAndLanguageHelper.GetMachineCurrentLocation();
            
            if (CheckRegion(region) == false)
            {
                Console.WriteLine("Your computer is not in an accepted region." +
                    " The only accepted regions are the United States, the United Kingdom, and Australia." +
                    " The application will now close.");

                Console.WriteLine("Press Enter to Exit");
                Console.ReadLine();

                Environment.Exit(0);
            }
                
            Read.ReadLines(region);

            Console.WriteLine("Press Enter to Exit");
            Console.ReadLine();
        }

        public static Boolean CheckRegion(string region)
        {
            if (region == "United States" || region == "United Kingdom" || region == "Australia")
                return true;
            else
                return false;
        }
    }
}
